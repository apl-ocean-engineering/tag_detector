#! /usr/bin/env python3

import time

import cv2
import numpy as np
import rospy
from cv_bridge import CvBridge
from sensor_msgs.msg import CompressedImage, Image

import apriltag


class TagDetector:
    def __init__(self) -> None:
        # ROS parameters (configurable through launch file)
        self.image_topic = rospy.get_param("~image_topic", "/image_raw/compressed")
        self.output_image_topic = rospy.get_param("~output_image_topic", "/annotations")
        self.marker_size = rospy.get_param("~marker_size", 0.127)

        self.aruco_dict_id = rospy.get_param("~aruco_dict_id", cv2.aruco.DICT_4X4_250)
        self.aruco_params = cv2.aruco.getPredefinedDictionary(self.aruco_dict_id)
        self.april_dict_id = rospy.get_param("~april_dict_id", "tag36h11")
        self.april_params = apriltag.DetectorOptions(
            families=self.april_dict_id,
            nthreads=4,
            quad_decimate=2.0,
            quad_blur=1.0,
            refine_edges=True,
            refine_decode=False,
            refine_pose=False,
            debug=False,
            quad_contours=False,
        )

        self.detector = apriltag.Detector(self.april_params)

        # Publishers
        self.image_pub = rospy.Publisher(self.output_image_topic, Image, queue_size=10)
        # OpenCV bridge
        self.bridge = CvBridge()

        # Subscribe to image topic
        image_sub = rospy.Subscriber(
            self.image_topic, CompressedImage, self.image_callback
        )

    def image_callback(self, data):
        image = self.bridge.compressed_imgmsg_to_cv2(data)
        if len(image.shape) == 3 and image.shape[2] == 3:
            gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            color_image = image
        else:
            gray_image = image
            color_image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)

        output_img = color_image.copy()

        # Aruco detection
        t0 = time.time()
        corners, ids, rejected = cv2.aruco.detectMarkers(color_image, self.aruco_params)
        t1 = time.time()

        # Draw detection results on the image
        if corners is not None and ids is not None:
            rospy.loginfo(
                f"{len(ids)} total ArucoTags detected in {1000*(t1 - t0):.3f}ms"
            )

            if len(corners) > 0:
                # Draw detected markers
                # cv2.aruco.drawDetectedMarkers(output_img, corners, ids)
                for i, corner in enumerate(corners):
                    # Get corner points as integers (round for thicker lines)
                    int_corner = np.intp(corner.reshape(-1, 2))

                    # Define thickness (adjust as desired)
                    thickness = 5

                    # Draw filled polygon for the marker (optional)
                    # cv2.fillPoly(image, [int_corner], (0, 255, 0))  # Green filling (BGR format)

                    # Draw the thick border lines
                    for j in range(4):
                        # Get starting and ending points for each line segment
                        start_point = int_corner[j]
                        end_point = int_corner[(j + 1) % 4]

                        # Draw the line segment with specified thickness
                        cv2.line(
                            output_img, start_point, end_point, (255, 0, 0), thickness
                        )  # Blue border (BGR format)
        else:
            rospy.loginfo_throttle(period=1, msg="No ArucoTags detected")

        t0 = time.time()
        results = self.detector.detect(gray_image)
        t1 = time.time()
        if len(results) > 0:
            rospy.loginfo(
                f"{len(results)} total AprilTags detected in {1000*(t1 - t0):.3f}ms"
            )

            for r in results:
                # extract the bounding box (x, y)-coordinates for the AprilTag
                # and convert each of the (x, y)-coordinate pairs to integers
                (ptA, ptB, ptC, ptD) = r.corners
                ptB = (int(ptB[0]), int(ptB[1]))
                ptC = (int(ptC[0]), int(ptC[1]))
                ptD = (int(ptD[0]), int(ptD[1]))
                ptA = (int(ptA[0]), int(ptA[1]))

                # draw the bounding box of the AprilTag detection
                cv2.line(output_img, ptA, ptB, (0, 255, 0), 5)
                cv2.line(output_img, ptB, ptC, (0, 255, 0), 5)
                cv2.line(output_img, ptC, ptD, (0, 255, 0), 5)
                cv2.line(output_img, ptD, ptA, (0, 255, 0), 5)

                # draw the center (x, y)-coordinates of the AprilTag
                (cX, cY) = (int(r.center[0]), int(r.center[1]))
                cv2.circle(output_img, (cX, cY), 5, (0, 0, 255), -1)
                # draw the tag family on the image
                tagFamily = r.tag_family.decode("utf-8")
                cv2.putText(
                    output_img,
                    tagFamily,
                    (ptA[0], ptA[1] - 15),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    0.5,
                    (0, 255, 0),
                    2,
                )
        else:
            rospy.loginfo_throttle(period=1, msg="No AprilTags detected")

        # Convert OpenCV image back to ROS image and publish
        ros_image = self.bridge.cv2_to_imgmsg(output_img, encoding="rgb8")
        self.image_pub.publish(ros_image)


if __name__ == "__main__":
    rospy.init_node("tag_detection_node")
    try:
        detector = TagDetector()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
