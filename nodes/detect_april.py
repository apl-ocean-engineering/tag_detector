#! /usr/bin/env python3

import cv2
import rospy
from cv_bridge import CvBridge
from sensor_msgs.msg import CompressedImage, Image

import apriltag


class AprilDetector:
    def __init__(self) -> None:
        # ROS parameters (configurable through launch file)
        self.image_topic = rospy.get_param("~image_topic", "/image_raw/compressed")
        self.output_image_topic = rospy.get_param(
            "~output_image_topic", "/april_annotation/image_raw"
        )

        self.april_dict_id = rospy.get_param("~april_dict_id", "tag36h11")
        self.april_params = apriltag.DetectorOptions(
            families=self.april_dict_id,
            nthreads=4,
            quad_decimate=2.0,
            quad_blur=1.0,
            refine_edges=True,
            refine_decode=False,
            refine_pose=False,
            debug=False,
            quad_contours=False,
        )
        self.detector = apriltag.Detector(self.april_params)
        self.marker_size = rospy.get_param("~marker_size", 0.127)

        # Publishers
        self.image_pub = rospy.Publisher(self.output_image_topic, Image, queue_size=10)

        # OpenCV bridge
        self.bridge = CvBridge()

        # Subscribe to image topic
        image_sub = rospy.Subscriber(
            self.image_topic, CompressedImage, self.image_callback
        )

    def image_callback(self, data):
        # image = self.bridge.imgmsg_to_cv2(data, desired_encoding="passthrough")
        image = self.bridge.compressed_imgmsg_to_cv2(data)

        if len(image.shape) == 3 and image.shape[2] == 3:
            gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            color_image = image
        else:
            gray_image = image
            color_image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
        output_img = color_image.copy()

        results = self.detector.detect(gray_image)

        if len(results) > 0:
            rospy.logdebug(f"{len(results)} total AprilTags detected")

            for r in results:
                # extract the bounding box (x, y)-coordinates for the AprilTag
                # and convert each of the (x, y)-coordinate pairs to integers
                (ptA, ptB, ptC, ptD) = r.corners
                ptB = (int(ptB[0]), int(ptB[1]))
                ptC = (int(ptC[0]), int(ptC[1]))
                ptD = (int(ptD[0]), int(ptD[1]))
                ptA = (int(ptA[0]), int(ptA[1]))

                # draw the bounding box of the AprilTag detection
                cv2.line(output_img, ptA, ptB, (0, 255, 0), 5)
                cv2.line(output_img, ptB, ptC, (0, 255, 0), 5)
                cv2.line(output_img, ptC, ptD, (0, 255, 0), 5)
                cv2.line(output_img, ptD, ptA, (0, 255, 0), 5)

                # draw the center (x, y)-coordinates of the AprilTag
                (cX, cY) = (int(r.center[0]), int(r.center[1]))
                cv2.circle(output_img, (cX, cY), 5, (0, 0, 255), -1)
                # draw the tag family on the image
                tagFamily = r.tag_family.decode("utf-8")
                cv2.putText(
                    output_img,
                    tagFamily,
                    (ptA[0], ptA[1] - 15),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    0.5,
                    (0, 255, 0),
                    2,
                )
        else:
            rospy.logdebug_throttle(period=1, msg="No AprilTags detected")
        ros_image = self.bridge.cv2_to_imgmsg(output_img, encoding="rgb8")
        self.image_pub.publish(ros_image)


if __name__ == "__main__":
    rospy.init_node("april_detection_node")
    try:
        april_detector = AprilDetector()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
