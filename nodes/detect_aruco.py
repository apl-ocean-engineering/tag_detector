#! /usr/bin/env python3

import cv2
import numpy as np
import rospy
from cv_bridge import CvBridge
from sensor_msgs.msg import CompressedImage, Image


class ArucoDetector:
    def __init__(self) -> None:
        # ROS parameters (configurable through launch file)
        self.image_topic = rospy.get_param("~image_topic", "/image_raw/compressed")
        self.output_image_topic = rospy.get_param(
            "~output_image_topic", "/aruco_annotation"
        )
        self.aruco_dict_id = rospy.get_param("~aruco_dict_id", cv2.aruco.DICT_4X4_250)
        self.aruco_params = cv2.aruco.getPredefinedDictionary(self.aruco_dict_id)
        self.marker_size = rospy.get_param("~marker_size", 0.127)

        # Publishers
        self.image_pub = rospy.Publisher(self.output_image_topic, Image, queue_size=10)
        # OpenCV bridge
        self.bridge = CvBridge()

        # Subscribe to image topic
        image_sub = rospy.Subscriber(
            self.image_topic, CompressedImage, self.image_callback
        )

    def image_callback(self, data):
        image = self.bridge.compressed_imgmsg_to_cv2(data)
        if len(image.shape) == 3 and image.shape[2] == 3:
            gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            color_image = image
        else:
            gray_image = image
            color_image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)

        output_img = color_image.copy()
        # Aruco detection
        corners, ids, rejected = cv2.aruco.detectMarkers(color_image, self.aruco_params)

        # Draw detection results on the image
        if corners is not None and ids is not None:
            rospy.loginfo(f"{len(ids)} total ArucoTags detected")

            if len(corners) > 0:
                # Draw detected markers
                # cv2.aruco.drawDetectedMarkers(output_img, corners, ids)
                for i, corner in enumerate(corners):
                    # Get corner points as integers (round for thicker lines)
                    int_corner = np.intp(corner.reshape(-1, 2))

                    # Define thickness (adjust as desired)
                    thickness = 5

                    # Draw filled polygon for the marker (optional)
                    # cv2.fillPoly(image, [int_corner], (0, 255, 0))  # Green filling (BGR format)

                    # Draw the thick border lines
                    for j in range(4):
                        # Get starting and ending points for each line segment
                        start_point = int_corner[j]
                        end_point = int_corner[(j + 1) % 4]

                        # Draw the line segment with specified thickness
                        cv2.line(
                            output_img, start_point, end_point, (255, 0, 0), thickness
                        )  # Blue border (BGR format)
        else:
            rospy.loginfo_throttle(period=1, msg="No ArucoTags detected")

        # Convert OpenCV image back to ROS image and publish
        ros_image = self.bridge.cv2_to_imgmsg(output_img, encoding="rgb8")
        self.image_pub.publish(ros_image)


if __name__ == "__main__":
    rospy.init_node("aruco_detection_node")
    try:
        aruco_detector = ArucoDetector()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
